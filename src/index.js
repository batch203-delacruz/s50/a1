import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Import the Bootstrap 5 css
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/*
  root.render() allows us to render/diplay reactjs elements and show it in the HTML document
*/
// const name = "John Smith";
// // const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }

// function formatName(fullName){
//   return `${fullName.firstName} ${fullName.lastName}`
// }

// const element = <h1>Hello, {formatName(user)}</h1>

// root.render(element);
