// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

import { useState, useContext } from "react"
import { NavLink } from "react-router-dom";
// shorthand method
import { Container, Nav, Navbar } from "react-bootstrap";

import UserContext from "../UserContext";

export default function AppNavBar(){

	// User state (global state) the will be used for the conditional rendering of our navbar

	const { user } = useContext(UserContext);
	console.log(user);
	/*
		"as" allows component to be treated as the component of the "react-router-dom" to gain access to its properties and functionalities.
		-"to" used in place of the href for providing the URL for the page

	*/
	return (
	     <Navbar bg="light" expand="lg">
	           <Container fluid>
	             <Navbar.Brand as={ NavLink } to="/">Zuitt</Navbar.Brand>
	             <Navbar.Toggle aria-controls="basic-navbar-nav" />
	             <Navbar.Collapse id="basic-navbar-nav">
	             {/*
					className us used instead of class to specify CSS class

					ml->ms (margin start)
					mr->me (margin end)

					If user is logged in, nav links visible
						-Home
						-Courses
						-Logout

					If not logged in,
						-Home
						-Courses
						-Login
						-Register
	             */}
	               <Nav className="ms-auto">
	                 <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
	                 <Nav.Link as={ NavLink } to="/courses" end>Courses</Nav.Link>
	                 {
	                 	(user.id !== null)
	                 	?
	                 		<Nav.Link as={ NavLink } to="/logout" end>Logout</Nav.Link>
	                 	:
	                 	<>
	                 		<Nav.Link as={ NavLink } to="/login" end>Login</Nav.Link>
	                 		<Nav.Link as={ NavLink } to="/register" end>Register</Nav.Link>
	                 	</>
	                 }
	               </Nav>
	             </Navbar.Collapse>
	           </Container>
	         </Navbar>
	       );
}