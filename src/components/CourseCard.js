/*
Mini Activity

    1. Create a "CourseCard" component showing a particular course with the name, description and price inside a React-Bootstrap Card.
        - Import the "Card" and "Button" in the react-bootstrap.
        - The "Card.Body" should contain the following:
            - The course name should be in the "Card.Title".
            - The "description" and "price" label should be in the "Card.Subtitle".
            - The value for "description" and "price" should be in the "Card.Text".
            - Add a "Button" with "primary" color for the Enroll.
    2. Create a "Courses.js" page and render the "CourseCard" component inside of it.
    3. Render also the "Courses" page in the parent component to mount/display it in our browser.
    4. Take a screenshot of your browser and send it in the batch hangouts

*/

import { useState, useEffect } from "react";

import { Card, Button } from "react-bootstrap";

import { Link } from "react-router-dom";

export default function CourseCard({courseProp}) {

	// console.log(props.courseProp.name);
	// console.log(typeof props);

	// console.log(courseProp);

	// Scenario: Keep track of the number of employees of each course

// Destructure the course properties into their own variables
	const { _id, name, description, price, slots } = courseProp;

	// Syntax:
	// const [stateName, setStateName] = useState(initialStateValue);

	// const [count, setCount] = useState(0);
	// const [seat, setSeat] = useState(30);
	// console.log(useState(10));

	// Function that keeps track of the enrollees for a course.

/*
	We will refactor the enroll function using useEffect hook to disable the button
*/
// 
	// const [disabled, setDisabled] = useState(false)

	// 

	// function unEnroll(){
	// 	setCount(count - 1);
	// }

	// function enroll(){
		
	// 	if (seat == 0 && count == 30)
	// 		{
	// 			alert(`No more seats!`);
	// 	}
	// 	else{
	// 		setCount(count + 1); 
	// 		console.log(`Enrollees: ${count}`);

	// 		setSeat(seat - 1)
	// 		console.log(`Seats: ${seat}`);
	// 	}
	
	// setCount(count + 1); 
	// console.log(`Enrollees: ${count}`);

	// setSeat(seat - 1)
	// console.log(`Seats: ${seat}`);
	// };

	// useEffect(() => {
	// 	if (seat <= 0){
	// 		setDisabled(true);
	// 		alert("No more seats available.");
	// 	}
	// },[seat]);

	return (
		<Card className="my-3">
		    <Card.Body>
		        <Card.Title>
		            {name}
		        </Card.Title>
		        	<Card.Subtitle>
		        		Description:
		        	</Card.Subtitle>
		        	<Card.Text>
		            	{description}
		        </Card.Text>

		        	<Card.Subtitle>
		        		Price:
		        	</Card.Subtitle>
		        	<Card.Text>
		            	Php {price}
		        </Card.Text>
		        <Card.Subtitle>
		        		slots:
		        </Card.Subtitle>
		        <Card.Text>
		        	{slots} available
		        </Card.Text>

		        <Button as = {Link} to = {`/courses/${_id}`} variant="primary">Details</Button>
		    </Card.Body>    
		</Card>
	)
}