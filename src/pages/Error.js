/*
s53 Activity Instructions:
1. Create a route which will be accessed when a user enters an undefined route and display the Error page.
2. Refactor the Banner component to be reusable for the Error page and the Home page.
a. Create an object variable named data that will contain the following properties:
- title
- description
-  destination
- label
b. Use the data variable as prop for the Banner that will be passed from the parent component (Home and Error page).

*/

import Banner from "../components/Banner";
	
const data = {
		title: "404 - Not Found",
		description: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back Home"
	}

export default function Error(){

	return (
		<Banner bannerProp={data}/>
	)
}