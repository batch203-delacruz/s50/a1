import { useEffect, useState } from "react";

import CourseCard from "../components/CourseCard";

// import coursesData from "../data/coursesData";

export default function Courses(){

// Course state that stores the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				return (
					<CourseCard key = {course._id} courseProp={course}/>
				);
			}))
		})
	}, []);





	// console.log(coursesData);

	// console.log(coursesData[0]);

	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key = {course.id} courseProp={course}/>
	// 	);
	// });

	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}