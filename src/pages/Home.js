import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

const data = {
	title: "Zuitt Coding Bootcamp",
	description: "Opportunities for everyone, everywhere",
	destination: "/courses",
	label: "Enroll Now"
}

export default function Home(){
	return (
		<>
			<Banner bannerProp={data} />
			<Highlights />
		</>
	)
}