/*
s52 Activity:
1. Create a Login page that simulates user login and authentication using an email and a password.
2. When all the input fields are filled, enable the submit button.
3. Upon clicking the submit button, a message will alert the user of a successful login.
*/


import { useState, useEffect, useContext } from "react";

import { Navigate } from "react-router-dom";

import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";


export default function Login() {

	


	// Consume the User Context obj and its values to use for user validation
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	// To gain access to methods that allow redirecting to another page
	// const navigate = useNavigate();


	useEffect(() => {
		if (email !=='' && password !=='') {
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])

	function loginUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type" :"application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.accessToken);

			if(data.accessToken !== undefined){
				localStorage.setItem("token", data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				});
			}
			else{
				Swal.fire({
					title: "Authentication Failed!",
					icon: "error",
					text: "Check your login details and try again."
				});

			}
		})

	


		// "localStorage" allows JS sites and apps to save key/value pairs in a web browser with no expiration date
		// localStorage.setItem("email", email);

		// setUser({
		// 	email: localStorage.getItem("email")
		// });

		setEmail('');
		setPassword('');

		// alert("You are now logged in.");

		// Redirects to homepage
		// navigate("/");
	}

	const retrieveUserDetails = (token) => {
		// Token will be sent as part of the request's header.

		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Change the global "user" state to store the id and isadmin prop

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});

		})
	}


	return(

		// Create a conditional statement that will redirect the user to the course page when a user is already logged in.

		(user.id !== null)
		?
			<Navigate to = "/courses"/>
		:
		<>
		<h1 className="my-5">Login</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group className="mb-3" controlId="emailAddress">
		    	<Form.Label>Email Address</Form.Label>
		    	<Form.Control 
		    		type="email" 
		    		placeholder="Enter email"
		    		value = {email}
		    		onChange={e => setEmail(e.target.value)}
		    		required
		    	/>
		  	</Form.Group>

		  	<Form.Group className="mb-3" controlId="password">
		    	<Form.Label>Password</Form.Label>
		    	<Form.Control 
		    	type="password" 
		    	placeholder="Enter password"
		    	value = {password}
		    	onChange={e => setPassword(e.target.value)}
		    	required
		    	/>
		  	</Form.Group>

		  	{
				isActive
				?
					<Button variant="success" type="submit" id="submitBtn">
					Login
					</Button>
				:

					<Button variant="success" type="submit" id="submitBtn" disabled>
					  Login
					</Button>
			}
		</Form>


		</>
	)
}