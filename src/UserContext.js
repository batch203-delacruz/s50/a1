// To give the logged in user object to have the "global" scope within the app

import React from "react";

// CONTEXT OBJECT
// Context Object with an object data type that can be used to store information that can be shared to other components within the app.
const UserContext = React.createContext();

// "Provider" allows other components to consume/use the context object and supply the needed info in the context obj
export const UserProvider = UserContext.Provider;

export default UserContext;